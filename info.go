package info

import (
	"fmt"
	"runtime"
)

var (
	Name      string
	Version   string
	GitRepo   string
	GitBranch string
	GitCommit string
	BuildDate string
	GoVersion = runtime.Version()
)

func String() string {
	info := `
	Name		: %s
	Version		: %s
	Git Repo	: %s
	Git Branch	: %s
	Git Commit	: %s
	Build Date	: %s
	Go Version	: %s
	`
	return fmt.Sprintf(
		info,
		Name,
		Version,
		GitRepo,
		GitBranch,
		GitCommit,
		BuildDate,
		GoVersion)
}
