package info

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"runtime"
	"strings"
)

type info struct {
	Name      string      `json:"name"`
	Version   string      `json:"version"`
	GitRepo   string      `json:"gitRepo"`
	GitBranch string      `json:"gitBranch"`
	GitCommit string      `json:"gitCommit"`
	BuildDate string      `json:"buildDate"`
	Runtime   runtimeInfo `json:"runtime"`
}

type runtimeInfo struct {
	GoVersion  string `json:"goVersion"`
	CPU        int    `json:"cpu"`
	Memory     string `json:"memory"`
	Goroutines int    `json:"goroutines"`
	GCCycles   uint32 `json:"gcCycles"`
	GCPauses   string `json:"gcPauses"`
}

func Middleware(endpoint string) func(http.Handler) http.Handler {
	f := func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			if (r.Method == "GET" || r.Method == "HEAD") && strings.EqualFold(r.URL.Path, endpoint) {
				renderJSON(w, http.StatusOK, buildInfo())
				return
			}
			h.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
	return f
}

func buildInfo() *info {
	m := new(runtime.MemStats)
	runtime.ReadMemStats(m)
	return &info{
		Name:      Name,
		Version:   Version,
		GitRepo:   GitRepo,
		GitBranch: GitBranch,
		GitCommit: GitCommit,
		BuildDate: BuildDate,
		Runtime: runtimeInfo{
			GoVersion:  GoVersion,
			CPU:        runtime.NumCPU(),
			Memory:     fmt.Sprintf("%.2fMB", float64(m.Alloc)/(1<<(10*2))),
			Goroutines: runtime.NumGoroutine(),
			GCCycles:   m.NumGC,
			GCPauses:   fmt.Sprintf("%dns", m.PauseTotalNs),
		},
	}
}

func renderJSON(w http.ResponseWriter, status int, data interface{}) {
	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(true)
	if err := enc.Encode(data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	w.Write(buf.Bytes()) //nolint:errcheck
}
